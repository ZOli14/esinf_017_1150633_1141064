/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import graph.AdjacencyMatrixGraph;
import graphbase.Graph;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1150633 e 1141064
 */
public class TheGameTest {

    public TheGameTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addLocal method, of class TheGame.
     */
    @Test
    public void testAddLocal() {
        System.out.println("TestAddLocal");
        TheGame game = new TheGame();
        Local l = new Local("LocalTeste", 350);
        int numeroLocais = game.getGraphAdj().numVertices();
        boolean result = game.addLocal(l);
        assertTrue("Resultado true", result == true);
        assertEquals(numeroLocais + 1, game.getGraphAdj().numVertices());
        assertEquals(game.searchNameLocal("LocalTeste"), l);

    }

    /**
     * Test of addCaminho method, of class TheGame.
     */
    @Test
    public void testAddCaminho() {
        System.out.println("TestAddCaminho");
        TheGame game = new TheGame();
        int numeroCaminhos = game.getGraphAdj().numEdges();
        Local l1 = new Local("Local1", 350);
        Local l2 = new Local("Local2", 300);
        game.addLocal(l1);
        game.addLocal(l2);
        boolean result = game.addCaminho("Local1", "Local2", 20.00);
        assertTrue("Resultado true", result == true);
        assertEquals(numeroCaminhos + 1, game.getGraphAdj().numEdges());
    }

    /**
     * Test of searchNameLocal method, of class TheGame.
     */
    @Test
    public void testSearchNameLocal() {
        System.out.println("TestSearchNameLocal");
        TheGame game = new TheGame();
        game.leituraLocaisCaminhos("inputLocais.txt");

        game.addLocal(new Local("Local1", 25));
        Local result = game.searchNameLocal("Local1");
        Local expResult = new Local("Local1", 25);
        assertEquals(expResult, result);
    }

    /**
     * Test of getGraphLoCa method, of class TheGame.
     */
    @Test
    public void testGetGraphLoCa() {
        System.out.println("getGraphLoCa");
        TheGame instance = new TheGame();
        AdjacencyMatrixGraph<Local, Double> expResult = new AdjacencyMatrixGraph<>();
        AdjacencyMatrixGraph<Local, Double> result = instance.getGraphAdj();
        assertEquals(expResult, result);
    }

    /**
     * Test of shortestPathCaminhos method, of class TheGame.
     */
    @Test
    public void testShortestPathCaminhos() {
        System.out.println("shortestPathCaminhos");
        TheGame instance = new TheGame();
        instance.leituraLocaisCaminhos("locais_S.txt");
        instance.leituraPersonagensAliancas("pers_S.txt");

        Local l1 = new Local("Local0", 30);
        Local l2 = new Local("Local1", 29);
        LinkedList<Local> expResult = new LinkedList<>();
        expResult.add(l1);
        expResult.add(l2);
        
        LinkedList<Local> result = TheGame.shortestPathCaminhos(l1, l2);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getGraphAdj method, of class TheGame.
     */
    @Test
    public void testGetGraphAdj() {
        System.out.println("getGraphAdj");
        TheGame instance = new TheGame();
        AdjacencyMatrixGraph<Local, Double> expResult = new AdjacencyMatrixGraph<>();
        AdjacencyMatrixGraph<Local, Double> result = instance.getGraphAdj();
        assertEquals(expResult, result);
    }

    /**
     * Test of addPersonagem method, of class TheGame.
     */
    @Test
    public void testAddPersonagem() {
        System.out.println("addPersonagem");
        Personagem p = new Personagem("ZOli", 30);
        TheGame instance = new TheGame();
        int num1 = instance.getGraphMap().numVertices();
        boolean expResult = true;
        boolean result = instance.addPersonagem(p);
        assertEquals(expResult, result);
        assertEquals(num1 + 1, instance.getGraphMap().numVertices());
    }

    /**
     * Test of addAlianca method, of class TheGame.
     */
    @Test
    public void testAddAlianca() {
        System.out.println("addAlianca");
        Personagem p1 = new Personagem("ZOli", 36);
        Personagem p2 = new Personagem("ILoz", 63);
        boolean privacidade = false;
        double compatibilidade = 0.5;
        TheGame instance = new TheGame();
        instance.addPersonagem(p1);
        instance.addPersonagem(p2);
        boolean expResult = true;
        boolean result = instance.addAlianca(p1, p2, privacidade, compatibilidade);
        assertEquals(expResult, result);
    }

    /**
     * Test of searchNamePersonagem method, of class TheGame.
     */
    @Test
    public void testSearchNamePersonagem() {
        System.out.println("searchNamePersonagem");
        String nome = "Pers1";
        TheGame instance = new TheGame();
        instance.leituraLocaisCaminhos("locais_S.txt");
        instance.leituraPersonagensAliancas("pers_S.txt");

        Personagem expResult = new Personagem(nome, 111);
        expResult.getListaLocais().add(instance.searchNameLocal("Local4"));
        Personagem result = instance.searchNamePersonagem(nome);
        assertEquals(expResult, result);
    }

    /**
     * Test of leituraPersonagensAliancas method, of class TheGame.
     */
    @Test
    public void testLeituraPersonagensAliancas() {
        System.out.println("leituraPersonagensAliancas");
        String fileName = "pers_S.txt";
        TheGame instance = new TheGame();

        int expResultPersonagens = 10;
        // como é NotDirected, em vez de 10 Edges, temos 20
        int expResultAliancas = 20;
        instance.leituraLocaisCaminhos("locais_S.txt");
        instance.leituraPersonagensAliancas(fileName);

        // numero de personagens / vertices
        assertEquals(instance.getGraphMap().numVertices(), expResultPersonagens);

        // numero de edges / alianças
        assertEquals(instance.getGraphMap().numEdges(), expResultAliancas);
    }

    /**
     * Test of getGraphMap method, of class TheGame.
     */
    @Test
    public void testGetGraphMap() {
        System.out.println("getGraphMap");
        TheGame instance = new TheGame();
        Graph<Personagem, Boolean> expResult = new Graph<>(false);
        Graph<Personagem, Boolean> result = instance.getGraphMap();
        assertEquals(expResult, result);
    }

    /**
     * Test of leituraLocaisCaminhos method, of class TheGame.
     */
    @Test
    public void testLeituraLocaisCaminhos() {
        System.out.println("leituraLocaisCaminhos");
        String fileName = "locais_S.txt";
        TheGame instance = new TheGame();
        instance.leituraLocaisCaminhos(fileName);

        Local expResult = new Local("Local0", 30);
        assertTrue(instance.getGraphAdj().checkVertex(expResult));
    }

    /**
     * Test of obterListaAliados method, of class TheGame.
     */
    @Test
    public void testObterListaAliados() {
        System.out.println("obterListaAliados");
        TheGame instance = new TheGame();
        instance.leituraLocaisCaminhos("locais_S.txt");
        instance.leituraPersonagensAliancas("pers_S.txt");
        Personagem p1 = instance.searchNamePersonagem("Pers0");

        LinkedList<Personagem> result = instance.obterListaAliados(p1);

        LinkedList<Personagem> expResult = new LinkedList<>();
        Personagem aliado1 = new Personagem("Pers3", 429);
        Personagem aliado2 = new Personagem("Pers4", 262);

        expResult.add(aliado1);
        expResult.add(aliado2);

        assertEquals(expResult, result);

    }

    /**
     * Test of aliancaMaisForte method, of class TheGame.
     */
    @Test
    public void testAliancaMaisForte() {
        System.out.println("aliancaMaisForte");
        TheGame instance = new TheGame();
        instance.leituraLocaisCaminhos("locais_S.txt");
        instance.leituraPersonagensAliancas("pers_S.txt");
        Map<Double, LinkedList<Personagem>> result = instance.aliancaMaisForte();
                
        Map<Double, LinkedList<Personagem>> expResult = new TreeMap<>();
        
        LinkedList<Personagem> personagens = new LinkedList<>();
        personagens.add(instance.searchNamePersonagem("Pers6"));
        personagens.add(instance.searchNamePersonagem("Pers8"));
        double strength = 962;

        expResult.put(strength, personagens);

        assertEquals(expResult, result);
    }
}
