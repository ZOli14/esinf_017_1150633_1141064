/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1150633 e 1141064
 */
public class Parte3Test {

    public Parte3Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of AreadFromFilePrefixos method, of class Parte3.
     */
    @Test
    public void testAreadFromFilePrefixos() {
        System.out.println("AreadFromFilePrefixos - Unidades");
        String filename1 = "poligonos_prefixo_unidades.txt";
        Parte3 instance = new Parte3();
        instance.AreadFromFilePrefixos(filename1);
        AVL<Poligono> expResult3 = new AVLPoligono();
        Poligono po = new Poligono("hena", 1);
        Poligono po2 = new Poligono("di", 2);
        Poligono po3 = new Poligono("tri", 3);
        Poligono po4 = new Poligono("tetra", 4);
        Poligono po5 = new Poligono("penta", 5);
        Poligono po6 = new Poligono("hexa", 6);
        Poligono po7 = new Poligono("hepta", 7);
        Poligono po8 = new Poligono("octa", 8);
        Poligono po9 = new Poligono("ennea", 9);
        expResult3.insert(po);
        expResult3.insert(po2);
        expResult3.insert(po3);
        expResult3.insert(po4);
        expResult3.insert(po5);
        expResult3.insert(po6);
        expResult3.insert(po7);
        expResult3.insert(po8);
        expResult3.insert(po9);
        System.out.println("1" + expResult3);
        System.out.println("2" + instance.getUm());
        assertTrue(expResult3.equals(instance.getUm()));

        System.out.println("AreadFromFilePrefixos - Dezenas");
        String filename2 = "poligonos_prefixo_dezenas.txt";
        Parte3 instance2 = new Parte3();
        instance2.AreadFromFilePrefixos(filename2);
        AVL<Poligono> expResult = new AVLPoligono();
        Poligono poli = new Poligono("deca", 10);
        Poligono poli2 = new Poligono("hendeca", 11);
        Poligono poli3 = new Poligono("dodeca", 12);
        Poligono poli4 = new Poligono("triskaideca", 13);
        Poligono poli5 = new Poligono("tetrakaideca", 14);
        Poligono poli6 = new Poligono("pentakaideca", 15);
        Poligono poli7 = new Poligono("hexakaideca", 16);
        Poligono poli8 = new Poligono("heptakaideca", 17);
        Poligono poli9 = new Poligono("octakaideca", 18);
        Poligono poli10 = new Poligono("enneakaideca", 19);
        Poligono poli11 = new Poligono("icosa", 20);
        Poligono poli12 = new Poligono("icosihena", 21);
        Poligono poli13 = new Poligono("icosidi", 22);
        Poligono poli14 = new Poligono("icositri", 23);
        Poligono poli15 = new Poligono("icositetra", 24);
        Poligono poli16 = new Poligono("icosipenta", 25);
        Poligono poli17 = new Poligono("icosihexa", 26);
        Poligono poli18 = new Poligono("icosihepta", 27);
        Poligono poli19 = new Poligono("icosiocta", 28);
        Poligono poli20 = new Poligono("icosiennea", 29);
        Poligono poli21 = new Poligono("triaconta", 30);
        Poligono poli22 = new Poligono("tetraconta", 40);
        Poligono poli23 = new Poligono("pentaconta", 50);
        Poligono poli24 = new Poligono("hexaconta", 60);
        Poligono poli25 = new Poligono("heptaconta", 70);
        Poligono poli26 = new Poligono("octaconta", 80);
        Poligono poli27 = new Poligono("enneaconta", 90);
        expResult.insert(poli);
        expResult.insert(poli2);
        expResult.insert(poli3);
        expResult.insert(poli4);
        expResult.insert(poli5);
        expResult.insert(poli6);
        expResult.insert(poli7);
        expResult.insert(poli8);
        expResult.insert(poli9);
        expResult.insert(poli10);
        expResult.insert(poli11);
        expResult.insert(poli12);
        expResult.insert(poli13);
        expResult.insert(poli14);
        expResult.insert(poli15);
        expResult.insert(poli16);
        expResult.insert(poli17);
        expResult.insert(poli18);
        expResult.insert(poli19);
        expResult.insert(poli20);
        expResult.insert(poli21);
        expResult.insert(poli22);
        expResult.insert(poli23);
        expResult.insert(poli24);
        expResult.insert(poli25);
        expResult.insert(poli26);
        expResult.insert(poli27);
        assertTrue(expResult.equals(instance2.getDez()));

        System.out.println("AreadFromFilePrefixos - Centenas");
        String nomeFicheiro3 = "poligonos_prefixo_centenas.txt";
        Parte3 instance3 = new Parte3();
        instance3.AreadFromFilePrefixos(nomeFicheiro3);
        AVL<Poligono> expResult2 = new AVLPoligono();
        Poligono pol = new Poligono("hecta", 100);
        Poligono pol2 = new Poligono("dihecta", 200);
        Poligono pol3 = new Poligono("trihecta", 300);
        Poligono pol4 = new Poligono("tetrahecta", 400);
        Poligono pol5 = new Poligono("pentahecta", 500);
        Poligono pol6 = new Poligono("hexahecta", 600);
        Poligono pol7 = new Poligono("heptahecta", 700);
        Poligono pol8 = new Poligono("octahecta", 800);
        Poligono pol9 = new Poligono("enneahecta", 900);
        expResult2.insert(pol);
        expResult2.insert(pol2);
        expResult2.insert(pol3);
        expResult2.insert(pol4);
        expResult2.insert(pol5);
        expResult2.insert(pol6);
        expResult2.insert(pol7);
        expResult2.insert(pol8);
        expResult2.insert(pol9);
        assertTrue(expResult2.equals(instance3.getCem()));
    }

    /**
     * Test of BnomePoligonoPorLados method, of class Parte3.
     */
    @Test
    public void testBnomePoligonoPorLados() {
        System.out.println("BnomePoligonoPorLados");
        int numLados = 524;
        Parte3 instance = new Parte3();
        instance.AreadFromFilePrefixos("poligonos_prefixo_dezenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_centenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_unidades.txt");

        String expResult = "pentahectaicositetragon";
        String result = instance.BnomePoligonoPorLados(numLados);
        assertEquals(expResult, result);
    }

    /**
     * Test of Carvore1a999 method, of class Parte3.
     */
    @Test
    public void testCarvore1a999() throws FileNotFoundException {
        System.out.println("Carvore1a999");
        Parte3 instance = new Parte3();
        instance.AreadFromFilePrefixos("poligonos_prefixo_dezenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_centenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_unidades.txt");
        
        AVLPoligono expResult = new AVLPoligono();
        
        Scanner ler = new Scanner(new File("teste_lados_nome.txt"));
        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            String[] split = linha.split(";");
            expResult.insert(new Poligono(Integer.parseInt(split[0]),split[1]));
        }
        
        AVLPoligono result = instance.Carvore1a999();
        assertTrue(expResult.equals(result));    
    }

    /**
     * Test of DladosPoligonoPorNome method, of class Parte3.
     */
    @Test
    public void testDladosPoligonoPorNome() {
        System.out.println("DladosPoligonoPorNome");
        String nome = "icosienneagon";
        Parte3 instance = new Parte3();
        instance.AreadFromFilePrefixos("poligonos_prefixo_dezenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_centenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_unidades.txt");
        int result = instance.DladosPoligonoPorNome(nome);
        
        int expResult = 29;
        assertEquals(expResult, result);
    }

    /**
     * Test of EconjuntoPoligonosEntreDoisValores method, of class Parte3.
     */
    @Test
    public void testEconjuntoPoligonosEntreDoisValores() {
        System.out.println("EconjuntoPoligonosEntreDoisValores");
        int n1 = 2;
        int n2 = 5;
        Parte3 instance = new Parte3();
        instance.AreadFromFilePrefixos("poligonos_prefixo_dezenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_centenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_unidades.txt"); 
        List<String> result = instance.EconjuntoPoligonosEntreDoisValores(n1, n2);

        List<String> expResult = new ArrayList<>();
        expResult.add("pentagon");
        expResult.add("tetragon");
        expResult.add("trigon");
        expResult.add("digon");
        assertEquals(expResult, result);
    }

    /**
     * Test of FantecessorComumMaisProximo method, of class Parte3.
     */
    @Test
    public void testFantecessorComumMaisProximo() {
        System.out.println("FantecessorComumMaisProximo");
        Poligono p = new Poligono("digon", 2);
        Poligono q = new Poligono("icosagon", 20);
        Parte3 instance = new Parte3();
        instance.AreadFromFilePrefixos("poligonos_prefixo_dezenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_centenas.txt");
        instance.AreadFromFilePrefixos("poligonos_prefixo_unidades.txt");

        Poligono expResult = new Poligono("hexakaidecagon", 16);
        Poligono result = instance.FantecessorComumMaisProximo(p, q);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUm method, of class Parte3.
     */
    @Test
    public void testGetUm() {
        System.out.println("getUm");
        Parte3 instance = new Parte3();
        AVLPoligono expResult = new AVLPoligono();
        AVLPoligono result = instance.getUm();
        assertTrue(expResult.equals(result));
    }

    /**
     * Test of getDez method, of class Parte3.
     */
    @Test
    public void testGetDez() {
        System.out.println("getDez");
        Parte3 instance = new Parte3();
        AVLPoligono expResult = new AVLPoligono();
        AVLPoligono result = instance.getDez();
        assertTrue(expResult.equals(result));
    }

    /**
     * Test of getCem method, of class Parte3.
     */
    @Test
    public void testGetCem() {
        System.out.println("getCem");
        Parte3 instance = new Parte3();
        AVLPoligono expResult = new AVLPoligono();
        AVLPoligono result = instance.getCem();
        assertTrue(expResult.equals(result));
    }

}
