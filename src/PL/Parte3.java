package PL;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1150633 e 1141064
 */
public class Parte3 {

    private AVLPoligono um;
    private AVLPoligono dez;
    private AVLPoligono cem;

    /**
     *Constructor
     */
    public Parte3() {
        um = new AVLPoligono();
        dez = new AVLPoligono();
        cem = new AVLPoligono();
    }

    /**
     * Lê o ficheiro e constroi três árvores balanceadas: uma para unidades (1~9), outra 
     * para dezenas (10~90) e outra para centenas (100~900).
     * @param fileName
     */
    public void AreadFromFilePrefixos(String fileName) {

        try {
            Scanner ler = new Scanner(new File(fileName));
            if (fileName.equalsIgnoreCase("poligonos_prefixo_unidades.txt")) {
                um = new AVLPoligono();
                while (ler.hasNextLine()) {
                    String line = ler.nextLine();
                    if (!line.isEmpty()) {
                        String[] leitor = line.split(";");

                        Poligono pol = new Poligono(Integer.parseInt(leitor[0]), leitor[1]);

                        getUm().insert(pol);
                    }
                }
            } else if (fileName.equalsIgnoreCase("poligonos_prefixo_dezenas.txt")) {
                dez = new AVLPoligono();
                while (ler.hasNextLine()) {
                    String line = ler.nextLine();
                    if (!line.isEmpty()) {
                        String[] leitor = line.split(";");

                        Poligono pol = new Poligono(Integer.parseInt(leitor[0]), leitor[1]);

                        getDez().insert(pol);
                    }
                }
            } else if (fileName.equalsIgnoreCase("poligonos_prefixo_centenas.txt")) {
                cem = new AVLPoligono();
                while (ler.hasNextLine()) {
                    String line = ler.nextLine();
                    if (!line.isEmpty()) {
                        String[] leitor = line.split(";");

                        Poligono pol = new Poligono(Integer.parseInt(leitor[0]), leitor[1]);

                        getCem().insert(pol);
                    }
                }
            }
            ler.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Parte3.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Com base nas três árvores anteriores elabora um método que devolve o nome 
     * de um polígono dado o seu número de lados. 
     * @param numLados
     * @return
     */
    public String BnomePoligonoPorLados(int numLados) {
        String nome_poligono = "";
        if (numLados == 0) {
            return null;
        }

        // 524 % 100 = 24   |   centenas = 524 - 24 = 500
        int centenas = numLados - (numLados % 100);

        // numLados % 100 = 24  |  24 % 10 = 4  | dezenas = 24 - 4 = 20
        int dezenas = (numLados % 100) - ((numLados % 100) % 10);

        // numLados % 100 = 24  |  24 % 10 = 4  |  unidades = 4
        int unidades = ((numLados % 100) % 10);

        if (centenas != 0) {
            for (Poligono pol : getCem().preOrder()) {
                if (centenas == pol.getLados()) {
                    nome_poligono = pol.getNome();
                    break;
                }
            }
        }

        if (dezenas != 0) {
            // verifica se é entre 10 e 30
            if (dezenas < 30) {
                dezenas = dezenas + unidades;
                for (Poligono pol : getDez().preOrder()) {
                    if (dezenas == pol.getLados()) {
                        nome_poligono += pol.getNome();
                        return nome_poligono + "gon";
                    }
                }
            } else {
                for (Poligono pol : getDez().preOrder()) {
                    if (dezenas == pol.getLados()) {
                        nome_poligono += pol.getNome();
                        break;
                    }
                }
            }
        }

        if (unidades != 0) {
            for (Poligono pol : getUm().preOrder()) {
                if (unidades == pol.getLados()) {
                    nome_poligono += pol.getNome();
                    break;
                }
            }
        }

        return nome_poligono + "gon";
    }

    /**
     *Constroi uma árvore balanceada que contenha todos os nomes de polígonos 
     * regulares de 1 até 999 lados.
     * @return
     */
    public AVLPoligono Carvore1a999() {
        int lados = 0;
        AVLPoligono result = new AVLPoligono();

        while (lados < 999) {
            lados++;
            result.insert(new Poligono(lados, BnomePoligonoPorLados(lados)));
        }
        return result;
    }

    /**
     * Devolve o número de lados de um polígono regular através do seu nome.
     * @param nome
     * @return
     */
    public int DladosPoligonoPorNome(String nome) {
        if (nome.isEmpty()) {
            return -1;
        }

        for (Poligono pol : Carvore1a999().preOrder()) {
            if (pol.getNome().equals(nome)) {
                return pol.getLados();
            }
        }

        return 0;
    }

    /**
     * Recebe um intervalo de numeros de lados, e devolve os correspondentes nomes dos
     * polígonos por ordem inversa, do maior para o menor número de lados.
     * @param n1
     * @param n2
     * @return
     */
    public List<String> EconjuntoPoligonosEntreDoisValores(int n1, int n2) {
        List<String> result = new ArrayList();
        for (int i = n2; i >= n1; i--) {
            result.add(BnomePoligonoPorLados(i));
        }
        return result;
    }

    /**
     * Recebe 2 polígonos encontrar e encontra o seu antecessor comum mais próximo) 
     * na árvore binária.
     * @param p1
     * @param p2
     * @return
     */
    public Poligono FantecessorComumMaisProximo(Poligono p1, Poligono p2) {
        return Carvore1a999().lowestCommonAncestor(p1, p2);
    }

    /**
     * @return the um
     */
    public AVLPoligono getUm() {
        return um;
    }

    /**
     * @return the dez
     */
    public AVLPoligono getDez() {
        return dez;
    }

    /**
     * @return the cem
     */
    public AVLPoligono getCem() {
        return cem;
    }
}
