/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

/**
 *
 * @author 1150633 e 1141064
 */
public class AVLPoligono extends AVL<Poligono> {
    
    public Poligono lowestCommonAncestor(Poligono p, Poligono q) {
        Node<Poligono> node1 = new Node<>(p, null, null);
        Node<Poligono> node2 = new Node<>(q, null, null);
        return lowestCommonAncestor(root, node1, node2).getElement();
    }

    private Node<Poligono> lowestCommonAncestor(Node<Poligono> root, Node<Poligono> p, Node<Poligono> q) {
        if (root == null) {
            return null;
        }
        if (root.getElement().compareTo(p.getElement()) == 0 || root.getElement().compareTo(q.getElement()) == 0) {
            return root;
        }
        Node<Poligono> l = lowestCommonAncestor(root.getLeft(), p, q);
        Node<Poligono> r = lowestCommonAncestor(root.getRight(), p, q);

        if (l != null && r != null) {
            return root;
        } else if (l == null && r == null) {
            return null;
        } else {
            return l == null ? r : l;
        }
    }
}
