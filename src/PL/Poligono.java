package PL;

import java.util.Objects;

/**
 *
 * @author 1150633 e 1141064
 */
public class Poligono implements Comparable<Poligono> {

    private int lados;
    private String nome;

    /**
     *
     * @param lados
     * @param nome
     */
    public Poligono(int lados, String nome) {
        this.lados = lados;
        this.nome = nome;
    }
    
    /**
     *
     * @param nome
     * @param lados
     */
    public Poligono (String nome, int lados) {
        this.lados = lados;
        this.nome = nome;
    }

    /**
     *
     */
    public Poligono() {
        lados = 0;
        nome = "";
    }

    /**
     * @return the lados
     */
    public int getLados() {
        return lados;
    }

    /**
     * @param lados the lados to set
     */
    public void setLados(int lados) {
        this.lados = lados;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Poligono other = (Poligono) obj;
        if (this.lados != other.lados) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Poligono{" + "lados=" + lados + ", nome=" + nome + '}';
    }

    @Override
    public int compareTo(Poligono p) {
        if (p.getLados() > lados) {
            return 1;
        } else if (p.getLados() < lados) {
            return -1;
        } else {
            return 0;
        }
    }
}
