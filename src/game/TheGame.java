package game;

import graph.EdgeAsDoubleGraphAlgorithms;
import graph.AdjacencyMatrixGraph;
import graphbase.Edge;
import graphbase.Graph;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1150633 e 1141064
 */
public class TheGame {

    private static AdjacencyMatrixGraph<Local, Double> graphAdj;
    private static Graph<Personagem, Boolean> graphMap;

    /**
     * Constructor 
     */
    public TheGame() {
        graphAdj = new AdjacencyMatrixGraph<>();
        graphMap = new Graph<>(false);
    }


    /**
     * Caminho com menor dificuldade (entre estradas)
     * @param l1
     * @param l2
     * @return
     */
    public static LinkedList<Local> shortestPathCaminhos(Local l1, Local l2) {
        LinkedList<Local> path = new LinkedList<Local>();
        EdgeAsDoubleGraphAlgorithms.shortestPath(graphAdj, l1, l2, path);
        return path;
    }

    /**
     * Adiciona local (vertex) ao grafo 
     * @param l
     * @return
     */
    public boolean addLocal(Local l) {
        return graphAdj.insertVertex(l);
    }

    /**
     * Adiciona um caminho (edge) ao garfo
     * @param nomeLocal1
     * @param nomeLocal2
     * @param dificuldade
     * @return
     */
    public boolean addCaminho(String nomeLocal1, String nomeLocal2, Double dificuldade) {
        if (graphAdj.vertices() != null) {
            Local l1 = searchNameLocal(nomeLocal1);
            Local l2 = searchNameLocal(nomeLocal2);
            return graphAdj.insertEdge(l1, l2, dificuldade);
        } else {
            throw new NullPointerException("Locais não encontrados!");
        }
    }

    /**
     * Procura pelo nome se um determinado local existe
     * @param nome
     * @return
     */
    public Local searchNameLocal(String nome) {
        if (graphAdj.numVertices() != 0) {
            for (Local l : graphAdj.vertices()) {
                if (nome.equalsIgnoreCase(l.getNome())) {
                    return l;
                }
            }
        }
        throw new NullPointerException("Não há locais registados!");
    }

    /**
     *
     * @return
     */
    public AdjacencyMatrixGraph<Local, Double> getGraphAdj() {
        return graphAdj;
    }

    /**
     *
     * @return
     */
    public Graph<Personagem, Boolean> getGraphMap() {
        return graphMap;
    }

    /**
     * Leitura do ficheiro que contem Locais e Caminhos
     * @param fileName
     */
    public void leituraLocaisCaminhos(String fileName) {
        try {
            String[] splitWord;
            Scanner ler = new Scanner(new File(fileName));
            char check = 'f';
            while (ler.hasNextLine()) {
                String line = ler.nextLine().trim();

                if (line.equalsIgnoreCase("locais")) {
                    check = 'l';
                }
                if (line.equalsIgnoreCase("caminhos")) {
                    check = 'c';
                }
                if (check == 'l' && !line.equalsIgnoreCase("locais")) {
                    splitWord = new String[2];
                    splitWord = line.split(",");
                    Local local = new Local(splitWord[0], Integer.parseInt(splitWord[1]));
                    addLocal(local);
                } else if (check == 'c' && !line.equalsIgnoreCase("caminhos")) {
                    splitWord = new String[3];
                    splitWord = line.split(",");

                    addCaminho(splitWord[0], splitWord[1], Double.parseDouble(splitWord[2]));
                }
            }
            ler.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TheGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Adiciona uma personagem
     * @param p
     * @return
     */
    public boolean addPersonagem(Personagem p) {
        return graphMap.insertVertex(p);
    }

    /**
     * Adiciona uma aliança
     * @param p1
     * @param p2
     * @param privacidade
     * @param compatibilidade
     * @return
     */
    public boolean addAlianca(Personagem p1, Personagem p2, boolean privacidade, double compatibilidade) {
        // verifica se os Personagens p1 e p2 são vertices do map
        if (!graphMap.validVertex(p1) || !graphMap.validVertex(p2)) {
            return false;
        }
        return graphMap.insertEdge(p1, p2, privacidade, compatibilidade);
    }

    /**
     * Procura uma personagem existente pelo nome e retorna null caso não exista
     * @param nome
     * @return
     */
    public Personagem searchNamePersonagem(String nome) {
        if (graphMap.numVertices() == 0) {
            return null;
        }
        for (Personagem p : graphMap.vertices()) {
            if (p.getNome().equals(nome)) {
                return p;
            }
        }

        return null;
    }
    
    /**
     * Leitura do ficheiro que contem Personagens e Alianças
     * @param fileName
     */
    public void leituraPersonagensAliancas(String fileName) {
        try {
            int i = 0, j = 0, count = 0;
            String[] splitWord;
            Scanner ler = new Scanner(new File(fileName));
            char check = 'f';
            while (ler.hasNextLine()) {
                String line = ler.nextLine().trim();
                count++;
                if (line.equalsIgnoreCase("personagens")) {
                    check = 'p';
                }
                if (line.equalsIgnoreCase("aliancas")) {
                    check = 'a';
                }

                if (check == 'p' && !line.equalsIgnoreCase("personagens")) {

                    splitWord = new String[3];
                    splitWord = line.split(",");
                    Personagem personagem = new Personagem(splitWord[0], Integer.parseInt(splitWord[1]));
                    Local loc = searchNameLocal(splitWord[2]);
                    personagem.getListaLocais().add(loc);
                    loc.setDono(personagem);
                    // atribui a um local, atualizando as devidas listas
                    // atribuiPersonagemALocal(personagem);
                    addPersonagem(personagem);

                } else if (check == 'a' && !line.equalsIgnoreCase("aliancas")) {

                    splitWord = new String[4];
                    splitWord = line.split(",");
                    if (splitWord[2].equalsIgnoreCase("true")) {
                        addAlianca(searchNamePersonagem(splitWord[0]), searchNamePersonagem(splitWord[1]), true, Double.parseDouble(splitWord[3]));
                    } else {
                        addAlianca(searchNamePersonagem(splitWord[0]), searchNamePersonagem(splitWord[1]), false, Double.parseDouble(splitWord[3]));
                    }
                }
            }
            ler.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TheGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // retorna o Local aleatório a que a personagem é atribuida
    // retorna null se não houver locais
    private Local atribuiPersonagemALocal(Personagem p) {
        if (graphAdj.numVertices() == 0) {
            return null;
        }

        Random rand = new Random();
        int local = rand.nextInt(graphAdj.numVertices());
        int i = 0;
        Local result = null;
        for (Local loca : graphAdj.vertices()) {
            if (i == local) {
                loca.setDono(p);
                result = loca;
            }

            i++;
        }

        p.getListaLocais().add(result);

        return result;
    }


    /**
     * 2B) - Devolve uma lista de aliados correspondetes as personagens dadas
     * @param p
     * @return
     */
    public LinkedList<Personagem> obterListaAliados(Personagem p) {
        LinkedList<Personagem> lisAliados = new LinkedList<>();
        System.out.println(p);
        for (Personagem personagem : graphMap.adjVertices(p)) {
            lisAliados.add(personagem);
        }
        return lisAliados;
    }

    /**
     * Cria uma nova aliança entre 2 personagens
     * @param p1
     * @param p2
     * @return
     */
    public boolean addAlianca(Personagem p1, Personagem p2) {
        if (!graphMap.validVertex(p1) || !graphMap.validVertex(p2)) {
            return false;
        }

        // verifica se já havia aliança
        boolean alianca = false;
        for (Personagem p : graphMap.adjVertices(p1)) {
            if (p.equals(p2)) {
                alianca = true;
            }
        }

        // nao têm alianca entre eles
        if (alianca = false) {
            double fator = Math.random();
            return graphMap.insertEdge(p1, p2, false, fator);
        }

        // ja tinham uma alianca entre eles
        // fazer shortestPath para contar Alianças em vez de Estradas
        return false;
    }

    /**
     * Retorna a aliança mais forte (a forca conjunta, e os membros da aliança)
     * @return
     */
    public Map<Double, LinkedList<Personagem>> aliancaMaisForte() {
        Map<Double, LinkedList<Personagem>> result = new TreeMap<>();
        LinkedList<Personagem> personagens = new LinkedList<>();
        double max = 0, forca;
        for (Edge<Personagem, Boolean> e : graphMap.edges()) {
            forca = e.getVDest().getForca() + e.getVDest().getForca();
            if (forca > max) {
                personagens.clear();
                personagens.add(e.getVOrig());
                personagens.add(e.getVDest());
                max = forca;
            }
        }

        result.put(max, personagens);

        return result;
    }

    /**
     * Determina se uma personagem pode conquistar um determinado local, devolvendo 
     * também a força necessária e a lista mínima de locais intermédios a conquistar
     * 
     * INCOMPLETO//
     * 
     * @param origin_name
     * @param dest_name
     * @param p_name
     * @return
     */
    public Map<Double, LinkedList<Local>> conquistarLocal(String origin_name, String dest_name, String p_name) {

        if (!graphAdj.checkVertex(searchNameLocal(origin_name)) || !graphAdj.checkVertex(searchNameLocal(dest_name)))
            return null;
        
        Map<Double, LinkedList<Local>> map = new TreeMap<>();
        LinkedList<Local> path = new LinkedList<>();

        Local origem = searchNameLocal(origin_name);
        Local destino = searchNameLocal(dest_name);
        Personagem p = searchNamePersonagem(p_name);

        //soma locais + somas caminhos ate local destino
        double forcaDestino = destino.getPontos();
        double pontos_persona = p.getForca();
        double forca_estrada = EdgeAsDoubleGraphAlgorithms.shortestPath(graphAdj, origem, destino, path);

        double forca_total = forca_estrada + forcaDestino;

        // verifica se o primeiro vértice pertence ao mesmo personagem
        if (path.get(1).getDono()==origem.getDono()) return null;
        
        // verifica se forca do destino é maior que a força da personagem 
        if (pontos_persona > forca_total) {
            map.put(forca_total, path);
        }

        return map;
    }
}
