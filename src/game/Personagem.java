package game;

import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author 1150633 e 1141064
 */
public class Personagem {

    private LinkedList<Local> listaLocais;
    private String nome;
    private int forca;

    /**
     *
     * @param nome
     * @param forca
     */
    public Personagem(String nome, int forca) {
        this.nome = nome;
        this.forca = forca;
        this.listaLocais = new LinkedList<>();
    }

    /**
     * @return the listaLocais
     */
    public LinkedList<Local> getListaLocais() {
        return listaLocais;
    }

    /**
     * @param listaLocais the listaLocais to set
     */
    public void setListaLocais(LinkedList<Local> listaLocais) {
        this.listaLocais = listaLocais;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the forca
     */
    public int getForca() {
        return forca;
    }

    /**
     * @param forca the forca to set
     */
    public void setForca(int forca) {
        this.forca = forca;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personagem other = (Personagem) obj;
        if (this.forca != other.forca) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Personagem{" + "nome=" + nome + ", forca=" + forca + '}';
    }

}
