package game;

import java.util.Objects;

/**
 *
 * @author 1150633 e 1141064
 */
public class Local {
        
    private String nome;
    private int pontos;
    private Personagem dono;

    /**
     *
     * @param nome
     * @param pontos
     */
    public Local(String nome, int pontos) {
        this.nome = nome;
        this.pontos = pontos;
        this.dono = null;
    }

    /**
     *
     * @return
     */
    public Personagem getDono() {
        return dono;
    }

    /**
     *
     * @param dono
     */
    public void setDono(Personagem dono) {
        this.dono = dono;
    }
 
    /**
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @return
     */
    public int getPontos() {
        return pontos;
    }

    /**
     *
     * @param pontos
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    @Override
    public String toString() {
        return String.format("\nNome=" + nome + "\nPontos=" + pontos);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Local other = (Local) obj;
        if (this.pontos != other.pontos) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        
        return true;
    }
    
    
    
    
}
